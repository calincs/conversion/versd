FROM node:18-alpine
RUN apk add --no-cache supervisor
RUN mkdir -p /home/node/api/node_modules \
  && chown -R node:node /home/node/api \
  && mkdir -p /home/node/ui/node_modules \
  && chown -R node:node /home/node/ui
USER node

WORKDIR /home/node/api
COPY --chown=node:node versd-api/package*.json ./
RUN npm install
WORKDIR /home/node/ui
COPY --chown=node:node versd-ui/package*.json ./
RUN npm install

WORKDIR /home/node/api
COPY --chown=node:node versd-api/*.js ./
COPY --chown=node:node versd-api/models models/
COPY --chown=node:node versd-api/controllers controllers/
COPY --chown=node:node versd-api/routes routes/

WORKDIR /home/node/ui
COPY --chown=node:node versd-ui/public public/
COPY --chown=node:node versd-ui/src src/
RUN npm run build

EXPOSE 8080 3000
COPY supervisord.conf /etc/supervisord.conf
CMD ["/usr/bin/supervisord"]