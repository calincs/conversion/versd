# Structured Vetting Environment

This repository consists of an API project and a UI project. Both projects are based on NodeJS. The API project uses a Postgres database ([ERD](versd-api/models/erd.png)) to store data and the UI project contains React components to render the user interface.

## Planning

Wireframes can be found at: <https://balsamiq.cloud/s7b9f6j/p61vxqh/r47FB>

Some initial thoughts and related documents can be found at:

- [Reconciliation API Wiki](https://gitlab.com/calincs/conversion/reconciliation-service/-/wikis/home)
- [Survey of available vetting entity linking tools](https://docs.google.com/document/d/1v-LjmEG_c8P74ZV81zcKqDeAiblHzsNbzRdrn-fXtYk/edit?ts=6001168e#)
- [Reconciliation API - Planning](https://docs.google.com/document/d/126nhlEg7k41RBOdXEjSOVm4JeQTKOkngkZ1I4ojUI_4/edit#)
- [Vetting Interface Planning](https://docs.google.com/document/d/1ER9VCoQsNYCYILPKe2is736KzL2GNnY3zKD2APxPzJg/edit#)

## Running Locally

Docker and git must first be installed on your machine.

```bash
git clone https://gitlab.com/calincs/conversion/structured-vetting-environment.git
cd structured-vetting-environment
docker-compose build
docker-compose up
```

The Postgres database will be persisted in a docker volume. The app will be available at [http://localhost:3000/](http://localhost:3000/).

## Developing Locally

git, npm, postgresql must be installed in your system.

```bash
git clone https://gitlab.com/calincs/conversion/structured-vetting-environment.git
```

Install node_modules for both API / UI

```bash
cd versd-api
npm install
```

```bash
cd versd-ui
npm install
```

Build projects (both have hot reload enabled)

```bash
cd versd-api
npm run dev
```

```bash
cd versd-ui
npm start
```

## Using the Service

You will need to have a Keycloak login in the appropriate environment. If developing locally, dev, otherwise staging/production.

To submit a job, navigate to the `Submit` page. There are a few options for how to format your data:

- JSON data within the `Data` field

```json
[
  {
    "unique_id": "TPatT_MODS_85.xml",
    "Wtitle1": "slash",
    "Wtitle2": "",
    "Wtitle3": "",
    "aTitle1": "armstrong jeannette",
    "aTitle2": "jeannette armstrong",
    "aTitle3": "",
    "bDay": 1948,
    "dDay": ""
  },
  {
    "unique_id": "TPatT_MODS_441.xml",
    "Wtitle1": "indian school days",
    "Wtitle2": "",
    "Wtitle3": "",
    "aTitle1": "basil johnston",
    "aTitle2": "johnston basil",
    "aTitle3": "",
    "bDay": "",
    "dDay": ""
  }
]
```

- Or a file uploaded with the `Upload File` field. Examples of the acceptable formats are available in the `versd-api/sample_data` directory.

Details for the permitted fields for each of the authorities are outlined [here](https://gitlab.com/calincs/conversion/reconciliation-service/-/wikis/Using-the-API).
