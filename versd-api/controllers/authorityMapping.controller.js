/** @format */

const db = require("../models");
const AuthorityMapping = db.authorityMapping;
const Op = db.Sequelize.Op;

// Create and Save a new AuthorityMapping
exports.create = (req, res) => {
  // Validate request
  const {
    jobId,
    sourceFieldHeading,
    authorityFieldHeading,
    authority,
    display,
    entity,
  } = req.body;

  if (!(jobId && authority && sourceFieldHeading && authorityFieldHeading)) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create a AuthorityMapping
  const authorityMapping = {
    jobId,
    authority,
    sourceFieldHeading,
    authorityFieldHeading,
    authority,
    display,
    entity,
  };
  // Save AuthorityMapping in the database
  return AuthorityMapping.create(authorityMapping)
    .then((data) => {
      console.log(
        ">> Created authorityMapping: " +
          JSON.stringify(authorityMapping, null, 4),
      );
      res.send(data);
    })
    .catch((err) => {
      console.log(">> Error while creating authorityMapping: ", err);
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating AuthorityMapping.",
      });
    });
};

// Retrieve all AuthorityMappings from the database.
exports.findAll = (req, res) => {
  AuthorityMapping.findAll({ where: req.query })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving authorityMappings.",
      });
    });
};

// Find a single AuthorityMapping with an id
exports.findOne = (req, res) => {
  const id = req.params.id;
  console.log(id);
  AuthorityMapping.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving AuthorityMapping with id=" + id,
      });
    });
};

// Update a AuthorityMapping by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  AuthorityMapping.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "AuthorityMapping was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update AuthorityMapping with id=${id}. Maybe AuthorityMapping was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating AuthorityMapping with id=" + id,
      });
    });
};

// Delete a AuthorityMapping with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  AuthorityMapping.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "AuthorityMapping was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete AuthorityMapping with id=${id}. Maybe AuthorityMapping was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete AuthorityMapping with id=" + id,
      });
    });
};

// Delete all AuthorityMappings from the database.
exports.deleteAll = (req, res) => {
  AuthorityMapping.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({
        message: `${nums} AuthorityMappings were deleted successfully!`,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while removing all authorityMappings.",
      });
    });
};
