/** @format */

const db = require("../models");
const CandidateMatch = db.candidateMatch;
const Op = db.Sequelize.Op;

// Create and Save a new CandidateMatch
exports.create = (req, res) => {
  // Validate request

  const { recordId, matchProb, data, authority, jobId, authorName, title } =
    req.body;
  if (!(recordId && matchProb && data && authority && jobId)) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create a CandidateMatch
  const candidateMatch = {
    recordId,
    matchProb,
    data,
    authority,
    jobId,
    authorName,
    title,
  };
  // Save CandidateMatch in the database
  return CandidateMatch.create(candidateMatch)
    .then((data) => {
      console.log(
        ">> Created candidateMatch: " + JSON.stringify(candidateMatch, null, 4),
      );
      res.send(data);
    })
    .catch((err) => {
      console.log(">> Error while creating candidateMatch: ", err);
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating CandidateMatch.",
      });
    });
};

// Retrieve all CandidateMatches from the database.
exports.findAll = (req, res) => {
  CandidateMatch.findAll({ where: req.query, order: [["matchProb", "DESC"]] })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving candidateMatches.",
      });
    });
};

// Find a single CandidateMatch with an id
exports.findOne = (req, res) => {
  const id = req.params.id;
  CandidateMatch.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving CandidateMatch with id=" + id,
      });
    });
};

// Update a CandidateMatch by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  CandidateMatch.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "CandidateMatch was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update CandidateMatch with id=${id}. Maybe CandidateMatch was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating CandidateMatch with id=" + id,
      });
    });
};

// Delete a CandidateMatch with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  CandidateMatch.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "CandidateMatch was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete CandidateMatch with id=${id}. Maybe CandidateMatch was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete CandidateMatch with id=" + id,
      });
    });
};

// Delete all CandidateMatchs from the database.
exports.deleteAll = (req, res) => {
  CandidateMatch.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({
        message: `${nums} CandidateMatches were deleted successfully!`,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while removing all candidateMatches.",
      });
    });
};
