/** @format */

const db = require("../models");
const Match = db.match;
const Op = db.Sequelize.Op;
const SourceRecord = db.sourceRecord;
const CandidateMatch = db.candidateMatch;
const Job = db.job;

async function getJobCountByUser(userId) {
  const jobCount = await Job.count({ where: { userId: userId } });
  return jobCount;
}

exports.countJobsByUser = (req, res) => {
  const userId = req.params.userId;

  getJobCountByUser(userId)
    .then((count) => res.send({ count }))
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving job counts.",
      });
    });
};

async function getSourceRecordCountByJob(jobId) {
  const sourceRecordCount = await SourceRecord.count({
    where: { jobId: jobId },
  });
  return sourceRecordCount;
}

exports.countSourceRecordsByJob = (req, res) => {
  const jobId = req.params.jobId;

  getSourceRecordCountByJob(jobId)
    .then((count) => res.send({ count }))
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving source record counts.",
      });
    });
};

async function getCandidateCountByJob(jobId) {
  const candidateCount = await CandidateMatch.count({
    where: { jobId: jobId },
  });
  return candidateCount;
}

exports.countCandidatesByJob = (req, res) => {
  const jobId = req.params.jobId;

  getCandidateCountByJob(jobId)
    .then((count) => res.send({ count }))
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving candidate counts.",
      });
    });
};

async function getMatchCountByJob(jobId) {
  // Get all sourceRecords' ids for a particular job
  const sourceRecords = await SourceRecord.findAll({
    attributes: ["id"],
    where: { jobId: jobId },
  });

  const sourceRecordIds = sourceRecords.map((record) => record.id);

  // Count matches
  const matchCount = await Match.count({
    where: { sourceRecordId: sourceRecordIds }, // Sequelize will use IN clause when sourceRecordIds is an array
  });

  return matchCount;
}

exports.countMatchesByJob = (req, res) => {
  const jobId = req.params.jobId;

  getMatchCountByJob(jobId)
    .then((count) => res.send({ count }))
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving match counts.",
      });
    });
};
