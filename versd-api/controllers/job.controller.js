const db = require("../models");
const Job = db.job;
const Op = db.Sequelize.Op;

// Create and Save a new Job
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create a Job
  const job = {
    name: req.body.name,
    status: "SUBMITTED",
    nssiJobId: req.body.nssiJobId,
    authorities: req.body.authorities,
    userId: req.body.userId,
    autoMatch: req.body.autoMatch
  };
  
  // Save Job in the database
  return Job.create(job)
    .then((data) => {
      console.log(">> Created job: " + JSON.stringify(job, null, 4));
      res.send(data);
    })
    .catch((err) => {
      console.log(">> Error while creating job: ", err);
      res.status(500).send({
        message: err.message || "Some error occurred while creating Job.",
      });
    });
};

// Retrieve all Jobs from the database.
exports.findAll = (req, res) => {
  Job.findAll({ where: req.query })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving jobs.",
      });
    });
};

// Find a single Job with an id
exports.findOne = (req, res) => {
  const id = req.params.id;
  console.log(id);
  Job.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(404).send({
        message: "Error retrieving Job with id=" + id,
      });
    });
};

// Update a Job by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;
  Job.update(req.body, {
    where: { id: id },
  })
    .then((success) => {
      if (success == 1) {
        res.status(200).send({
          message: "Job was updated successfully.",
        });
      } else {
        res.status(404).send({
          message: `Cannot update Job with id=${id}. Maybe Job was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({
        message: "Error updating Job with id=" + id,
      });
    });
};

// Delete a Job with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Job.destroy({
    where: { id: id },
  })
    .then((success) => {
      if (success == 1) {
        res.send({
          message: "Job was deleted successfully!",
        });
      } else {
        res.status(404).send({
          message: `Cannot delete Job with id=${id}. Maybe Job was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Job with id=" + id,
      });
    });
};

// Delete all Jobs from the database.
exports.deleteAll = (req, res) => {
  Job.destroy({
    where: {},
    truncate: false,
  })
    .then((count) => {
      res.send({ message: `${count} Jobs were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while removing all jobs.",
      });
    });
};

// Find all Jobs with given status
exports.findAllByStatus = (req, res) => {
  const status = req.params.status;
  Job.findAll({ where: { status: status } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving jobs.",
      });
    });
};
exports.findAllByUser = (req, res) => {
  const userId = req.params.userId;
  Job.findAll({ where: { userId: userId } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving jobs.",
      });
    });
};