/** @format */

const db = require("../models");
const Match = db.match;
const Op = db.Sequelize.Op;
const SourceRecord = db.sourceRecord;

// Create and Save a new Match
exports.create = (req, res) => {
  // Validate request

  const {
    sourceRecordId,
    sourceEntityHeading,
    sourceEntityValue,
    matchEntityID,
    status,
    relationship,
  } = req.body;

  if (
    !(
      sourceRecordId &&
      sourceEntityHeading &&
      sourceEntityValue &&
      matchEntityID &&
      status &&
      relationship
    )
  ) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create a Match
  const match = {
    sourceRecordId,
    sourceEntityHeading,
    sourceEntityValue,
    matchEntityID,
    status,
    relationship,
  };
  // Save Match in the database
  return Match.create(match)
    .then((data) => {
      console.log(">> Created match: " + JSON.stringify(match, null, 4));
      res.send(data);
    })
    .catch((err) => {
      console.log(">> Error while creating match: ", err);
      res.status(500).send({
        message: err.message || "Some error occurred while creating Match.",
      });
    });
};

// Retrieve all Matches from the database.
exports.findAll = (req, res) => {
  Match.findAll({ where: req.query })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving matchs.",
      });
    });
};

// Find a single Match with an id
exports.findOne = (req, res) => {
  const {
    sourceRecordId,
    sourceEntityHeading,
    sourceEntityValue,
    matchEntityID,
  } = req.params;

  Match.findOne({
    where: {
      sourceRecordId: sourceRecordId,
      sourceEntityHeading: sourceEntityHeading,
      sourceEntityValue: sourceEntityValue,
      matchEntityID: matchEntityID,
    },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving Match with id=" + id,
      });
    });
};

// Update a Match by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Match.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Match was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update Match with id=${id}. Maybe Match was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Match with id=" + id,
      });
    });
};

// Delete a Match with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Match.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Match was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete Match with id=${id}. Maybe Match was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Match with id=" + id,
      });
    });
};

// Delete all Matches from the database.
exports.deleteAll = (req, res) => {
  Match.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} Matches were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all matchs.",
      });
    });
};

// Find all Matches with given status
exports.findAllByStatus = (req, res) => {
  const status = req.params.status;
  Match.findAll({ where: { status: status } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving matches.",
      });
    });
};
exports.findAllByUser = (req, res) => {
  const userId = req.params.userId;
  Match.findAll({ where: { userId: userId } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving matches.",
      });
    });
};
