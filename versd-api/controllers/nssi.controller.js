/** @format */

var axios = require("axios");

const tokenConfig = () => {
  var qs = require("qs");
  var data = qs.stringify({
    client_secret: process.env.API_CLIENT_SECRET,
    grant_type: "client_credentials",
    client_id: "versd-api",
  });
  var config = {
    method: "post",
    url: `${process.env.KC_SERVER_AUTH_URL}realms/lincs/protocol/openid-connect/token`,
    headers: {
      Authorization: "Basic Og==",
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: data,
  };
  return config;
};

const requestConfig = (method = "post", endpoint, token, data = null) => {
  var config = {
    method: method,
    url: endpoint,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    data: JSON.stringify(data),
  };
  return config;
};

exports.token = (req, res) => {
  const config = tokenConfig();

  axios(config)
    .then(function (response) {
      res.send(JSON.stringify(response.data));
    })
    .catch(function (error) {
      console.log(error);
    });
};

exports.submit = async (req, res) => {
  const config = tokenConfig();
  const url = `${process.env.NSSI_URL}jobs`;
  try {
    const tokResponse = await axios(config);
    const token = tokResponse.data.access_token;
    const response = await axios(requestConfig("post", url, token, req.body));
    res.send(response.data);
  } catch (error) {
    console.error(error);
  }
};

exports.status = async (req, res) => {
  const config = tokenConfig();
  const url = `${process.env.NSSI_URL}jobs/${req.params.id}`;
  try {
    const tokResponse = await axios(config);
    const token = tokResponse.data.access_token;
    const response = await axios(requestConfig("get", url, token));
    res.send(response.data);
  } catch (error) {
    res.send(error);
  }
};

exports.results = async (req, res) => {
  const config = tokenConfig();
  const url = `${process.env.NSSI_URL}results/reconciliation/${req.params.id}`;
  try {
    const tokResponse = await axios(config);
    const token = tokResponse.data.access_token;
    const response = await axios(requestConfig("get", url, token));
    res.send(response.data);
  } catch (error) {
    console.log(error);
  }
};
