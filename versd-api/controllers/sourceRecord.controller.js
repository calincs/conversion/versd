const db = require("../models");
const SourceRecord = db.sourceRecord;
const Op = db.Sequelize.Op;

// Create and Save a new SourceRecord
exports.create = (req, res) => {
  // Validate request

  const { jobId, recordId, data } = req.body;
  if (!(jobId && recordId && data)) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create a SourceRecord
  const sourceRecord = {
    jobId,
    recordId,
    data,
  };
  // Save SourceRecord in the database
  return SourceRecord.create(sourceRecord)
    .then((data) => {
      // console.log(">> Created sourceRecord: " + JSON.stringify(sourceRecord, null, 4));
      res.send(data);
    })
    .catch((err) => {
      console.log(">> Error while creating sourceRecord: ", err);
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating SourceRecord.",
      });
    });
};

// Retrieve all SourceRecords from the database.
exports.findAll = (req, res) => {
  SourceRecord.findAll({ where: req.query })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving sourceRecords.",
      });
    });
};

// Find a single SourceRecord with an id
exports.findOne = (req, res) => {
  const id = req.params.id;
  SourceRecord.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving SourceRecord with id=" + id,
      });
    });
};
// Find a single SourceRecord with an id
exports.findAllByJob = (req, res) => {
  const id = req.params.id;
  SourceRecord.findAll({ where: { jobId: id } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving SourceRecord with id=" + id,
      });
    });
};

// Update a SourceRecord by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  SourceRecord.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "SourceRecord was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update SourceRecord with id=${id}. Maybe SourceRecord was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating SourceRecord with id=" + id,
      });
    });
};

// Delete a SourceRecord with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  SourceRecord.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "SourceRecord was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete SourceRecord with id=${id}. Maybe SourceRecord was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete SourceRecord with id=" + id,
      });
    });
};

// Delete all SourceRecords from the database.
exports.deleteAll = (req, res) => {
  SourceRecord.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} SourceRecords were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while removing all sourceRecords.",
      });
    });
};

