/** @format */

const db = require("../models");
const UserRole = db.userRole;
const Op = db.Sequelize.Op;

// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  const { userId, jobId, role } = req.body;

  if (!(userId && jobId && role)) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create a User
  const userRole = {
    userId,
    jobId,
    role,
  };

  // Save User in the database
  return UserRole.create(userRole)
    .then((data) => {
      console.log(">> Created user: " + JSON.stringify(userRole, null, 4));
      res.send(data);
    })
    .catch((err) => {
      console.log(">> Error while creating userRole: ", err);
      res.status(500).send({
        message: err.message || "Some error occurred while creating UserRole.",
      });
    });
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {
  UserRole.findAll({ where: req.query })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving users.",
      });
    });
};

// Find a single User with an id
exports.findOne = (req, res) => {
  const { userId, jobId } = req.params;

  UserRole.findOne({
    where: {
      userId,
      jobId,
    },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id,
      });
    });
};

// Update a User by the id in the request
exports.update = (req, res) => {
  const { userId, jobId } = req.params;

  UserRole.update(req.body, {
    where: { userId, jobId },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "UserRole was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update UserRole with jobId=${jobId} & userId=${userId}. Maybe UserRole was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error updating UserRole with jobId=${jobId} & userId=${userId}`,
      });
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  const { userId, jobId } = req.params;

  UserRole.destroy({
    where: { userId, jobId },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "UserRole was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete UserRole with jobId=${jobId} & userId=${userId}. Maybe UserRole was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Could not delete UserRole with jobId=${jobId} & userId=${userId}`,
      });
    });
};

// Delete all Users from the database.
exports.deleteAll = (req, res) => {
  UserRole.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} UserRoles were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all UserRoles.",
      });
    });
};

// Find all Users with given status
exports.findAllByRole = (req, res) => {
  const role = req.params.role;
  UserRole.findAll({ where: { role: role } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving UserRoles.",
      });
    });
};
exports.findAllByUser = (req, res) => {
  const userId = req.params.userId;
  UserRole.findAll({ where: { userId: userId } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving UserRoles.",
      });
    });
};
