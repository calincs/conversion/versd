var session = require("express-session");
var Keycloak = require("keycloak-connect");

let keycloak;

var keycloakConfig = {
  realm: "lincs",
  "bearer-only": true,
  "auth-server-url": "https://keycloak.dev.lincsproject.ca/",
  "ssl-required": "external",
  resource: "versd-api",
  "verify-token-audience": true,
  "use-resource-role-mappings": true,
  "confidential-port": 0,
};

function initKeycloak() {
  if (keycloak) {
    console.log("keycloak instance: ", keycloak);
    return keycloak;
  } else {
    console.log("Initalizing keycloak...");
    var memoryStore = new session.memoryStore();
    keycloak = new Keycloak(
      {
        store: memoryStore,
        secret: "any_key",
        resave: false,
        saveUninitialized: true,
      },
      keycloakConfig
    );
    return keycloak;
  }
}

module.exports = {
  initKeycloak,
};
