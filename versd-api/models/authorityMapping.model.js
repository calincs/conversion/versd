/** @format */

const { DataTypes, STRING } = require("sequelize");

module.exports = (sequelize) => {
  //Timestamp removed as createdAt, updatedAt are automatically added to all models
  const AuthorityMapping = sequelize.define("authorityMapping", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    sourceFieldHeading: {
      type: DataTypes.STRING,
    },
    authorityFieldHeading: {
      type: DataTypes.STRING,
    },
    authority: {
      type: DataTypes.STRING,
    },

    //TODO: Review if useful or should belong as part of the datatable
    display: {
      type: DataTypes.BOOLEAN,
      default: true,
    },

    // TODO: review purpose of below field
    entity: {
      type: DataTypes.BOOLEAN,
    },
  },
  {
    schema: "public",
  });

  return AuthorityMapping;
};
