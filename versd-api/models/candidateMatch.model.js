/** @format */

const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  const CandidateMatch = sequelize.define("candidateMatch", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    matchProb: {
      type: DataTypes.FLOAT,
    },
    data: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    authority: {
      //TODO: Review if this is useful information to have
      type: DataTypes.STRING,
    },
    recordId: {
      type: DataTypes.STRING,
    },
    // Possibly make this more generic for other data sources: ex. candidatetype and sortField1 and sortField2?
    authorName: {
      type: DataTypes.STRING,
    },
    title: {
      type: DataTypes.STRING,
    },
  },
  {
    freezeTableName: true,
  });

  return CandidateMatch;
};
