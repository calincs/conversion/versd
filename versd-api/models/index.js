/** @format */

const { Sequelize } = require("sequelize");
const dotenv = require("dotenv");
dotenv.config();

let sequelize = null;
if (!process.env.DATABASE_URL) {
  var config = {
    username: process.env.USERNAME ?? "dbuser",
    password: process.env.PASSWORD ?? "dbsecret",
    database: process.env.DB ?? "database",
    dialect: process.env.DIALECT ?? "postgres",
    host: process.env.HOST,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  };
  sequelize = new Sequelize(config);
} else {
  sequelize = new Sequelize(process.env.DATABASE_URL);
}

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

// Assign Models
db.job = require("./job.model.js")(sequelize, Sequelize);
db.user = require("./user.model.js")(sequelize, Sequelize);
db.sourceRecord = require("./sourceRecord.model.js")(sequelize, Sequelize);
db.authorityMapping = require("./authorityMapping.model.js")(
  sequelize,
  Sequelize,
);
db.match = require("./match.model.js")(sequelize, Sequelize);
db.candidateMatch = require("./candidateMatch.model.js")(sequelize, Sequelize);
db.userRole = require("./userRole.model.js")(sequelize, Sequelize);

// Adding associations between models
// db.job.belongsTo(db.userRole)
db.job.hasMany(db.userRole, { foreignKey: "jobId" });
db.userRole.belongsTo(db.job, { foreignKey: "jobId" });

db.job.hasMany(db.sourceRecord);
db.sourceRecord.belongsTo(db.job);

db.job.hasMany(db.authorityMapping);
db.authorityMapping.belongsTo(db.job);

db.sourceRecord.hasMany(db.match);
db.match.belongsTo(db.sourceRecord);

//NOTE: Removed association between: db.sourceRecord & db.candidateMatch because recordIds won't be known when candidates are found
// db.sourceRecord.hasMany();
// db.candidateMatch.belongsTo(db.sourceRecord);

db.job.hasMany(db.candidateMatch);
db.candidateMatch.belongsTo(db.job);

sequelize.sync();

module.exports = db;
