const { DataTypes } = require("sequelize");

/* 
  This model represents the metadata of a job that will be submitted to Reconciliation Service
  May want to add filename as a field if applicable?
  
*/
module.exports = (sequelize) => {
  const Job = sequelize.define("job",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      userId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      nssiJobId: {
        // can't make composite key since value will be null until value returned from nssi or value will be list
        type: DataTypes.STRING,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      filename: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      status: {
        type: DataTypes.ENUM,
        values: ["SUBMITTED", "READY", "PROCESSING", "FAILED"],
      },
      authorities: DataTypes.STRING,
      autoMatch: {
        type: DataTypes.BOOLEAN,
        default: false,
      },
    },
    {
      schema: "public",
    }
  );

  return Job;
};
