/** @format */

const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  //Timestamp removed as createdAt, updatedAt are automatically added to all models
  const Match = sequelize.define(
    "match",
    {
      // fk: sourceRecordId
      sourceEntityHeading: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      sourceEntityValue: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      matchEntityID: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      candidateID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      status: {
        type: DataTypes.STRING,
        default: "vetted",
        // Change to enum once we've settled on statuses
      },
      relationship: {
        type: DataTypes.STRING,
        default: "same as",
      },
    },
    {
      schema: "public",
    },
  );

  return Match;
};
