const db = require("./index");
exports.seedUsers = () => {
  db.user
    .bulkCreate([
      {
        id: 1,
        username: "testuser",
        email: "testuser@lincs.ca",
        password: "test",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: 2,
        username: "temp",
        email: "tempuser1@lincs.ca",
        password: "test",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ])
    .then(() => {
      return db.user.findAll();
    })
    .then((users) => {
      // console.log(users);
    });
};

exports.seedJobs = () => {
  db.job
    .bulkCreate([
      {
        id: 900,
        userId: "temp",
        nssiJobId: "17",
        name: "Demo job",
        status: "READY",
        authorities: "viaf-works",
        autoMatch: false,
      },
      {
        id: 902,
        userId: "temp",
        nssiJobId: "16,17",
        name: "Demo job (2 jobs ready)",
        status: "SUBMITTED",
        authorities: "viaf-works",
        autoMatch: false,
      },
      {
        id: 801,
        userId: "temp",
        nssiJobId: "20",
        name: "test job",
        status: "SUBMITTED",
        authorities: "viaf-works",
        autoMatch: false,
      },
      {
        id: 799,
        userId: "temp",
        nssiJobId: "15",
        name: "failed job :(",
        status: "FAILED",
        authorities: "viaf-works",
        autoMatch: false,
      },
      {
        id: 719,
        userId: "temp",
        nssiJobId: "15,16",
        name: "multiple jobs fail/ready :(",
        status: "SUBMITTED",
        authorities: "viaf-works",
        autoMatch: false,
      },
      {
        id: 720,
        userId: "temp",
        nssiJobId: "16,20",
        name: "multiple jobs ready/in progress :(",
        status: "SUBMITTED",
        authorities: "viaf-works",
        autoMatch: false,
      },
    ])
    .then(() => {
      return db.job.findAll();
    })
    .then((jobs) => {
      // console.log(users);
    });
}


exports.seedCandidates = () => {
  db.candidateMatch
    .bulkCreate([
      {
        id: 800,
        userId: "temp",
        nssiJobId: "60",
        name: "Demo job",
        status: "READY",
        authorities: "viaf-works",
        autoMatch: false,
      },
      {
        id: 901,
        userId: "temp",
        nssiJobId: "66",
        name: "test job",
        status: "SUBMITTED",
        authorities: "viaf-works",
        autoMatch: false,
      },
      {
        id: 799,
        userId: "temp",
        nssiJobId: "68",
        name: "failed job :(",
        status: "FAILED",
        authorities: "viaf-works",
        autoMatch: false,
      },
    ])
    .then(() => {
      return db.job.findAll();
    })
    .then((jobs) => {
      // console.log(users);
    });
}
