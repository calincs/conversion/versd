const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  //Timestamp removed as createdAt, updatedAt are automatically added to all models
  const SourceRecord = sequelize.define("sourceRecord", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    recordId: {
      type: DataTypes.STRING,
    },
    data: {
      type: DataTypes.JSON,
      allowNull: false,
    },
  },
  {
    schema: "public",
  });

  return SourceRecord;
};
