/** @format */

const { DataTypes } = require("sequelize");

// Keeping this for additional user functions

module.exports = (sequelize) => {
  const User = sequelize.define(
    "user",
    {
      id: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        primaryKey: true,
      },

      settings: {
        type: DataTypes.JSON,
        allowNull: true,
        default: null,
      },
    },
    {
      schema: "public",
    },
  );

  return User;
};
