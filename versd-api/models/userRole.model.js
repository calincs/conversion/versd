/** @format */

const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  const UserRole = sequelize.define(
    "userRole",
    {
      userId: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
      },
      jobId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        references: {
          model: "jobs", // 'jobs' because Sequelize pluralizes by default
          key: "id",
        },
      },

      role: {
        type: DataTypes.ENUM,
        values: ["CREATOR", "CONTRIBUTOR"], //REMOVED ADMIN SINCE THAT WILL BE GRANTED FROM KEYCLOAK
        allowNull: false,
      },
    },
    {
      schema: "public",
    },
  );

  return UserRole;
};
