module.exports = app => {
    const authorities = require("../controllers/authority.controller.js");

    var router = require("express").Router();

    // Create a new Job
    router.post("/", authorities.create);

    // Retrieve all Jobs
    router.get("/", authorities.findAll);

    // Retrieve a single Job with id
    router.get("/:id", authorities.findOne);

    // Update a Job with id
    router.put("/:id", authorities.update);

    // Delete a Job with id
    router.delete("/:id", authorities.delete);

    // Create a new Job
    router.delete("/", authorities.deleteAll);

    app.use('/api/authorities', router);
};