module.exports = app => {
    const authorityMappings = require("../controllers/authorityMapping.controller.js");

    var router = require("express").Router();

    // Create a new Job
    router.post("/", authorityMappings.create);

    // Retrieve all Jobs
    router.get("/", authorityMappings.findAll);

    // Retrieve a single Job with id
    router.get("/:id", authorityMappings.findOne);

    // Update a Job with id
    router.put("/:id", authorityMappings.update);

    // Delete a Job with id
    router.delete("/:id", authorityMappings.delete);

    // Create a new Job
    router.delete("/", authorityMappings.deleteAll);

    app.use('/api/authorityMappings', router);
};