module.exports = app => {
    const candidateMatches = require("../controllers/candidateMatch.controller.js");

    var router = require("express").Router();

    // Create a new Job
    router.post("/", candidateMatches.create);

    // Retrieve all Jobs
    router.get("/", candidateMatches.findAll);

    // Retrieve a single Job with id
    router.get("/:id", candidateMatches.findOne);

    // Update a Job with id
    router.put("/:id", candidateMatches.update);

    // Delete a Job with id
    router.delete("/:id", candidateMatches.delete);

    // Create a new Job
    router.delete("/", candidateMatches.deleteAll);

    app.use('/api/candidateMatches', router);
};