/** @format */

module.exports = (app) => {
  const counts = require("../controllers/counts.controller.js");

  var router = require("express").Router();

  // Counts of matches by job
  router.get("/matches/:jobId", counts.countMatchesByJob);

  // Counts of candidates by job
  router.get("/candidates/:jobId", counts.countCandidatesByJob);

  // Counts of sourceRecord by job
  router.get("/sourceRecords/:jobId", counts.countSourceRecordsByJob);

  // Counts of jobs by userId
  router.get("/jobs/:userId", counts.countJobsByUser);

  app.use("/api/counts", router);
};
