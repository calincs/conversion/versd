/** @format */

module.exports = (app) => {
  const matches = require("../controllers/match.controller.js");

  var router = require("express").Router();

  // Create a new Match
  router.post("/", matches.create);

  // Retrieve all Matchs
  router.get("/", matches.findAll);

  // Retrieve a single Match with id
  router.get("/:id", matches.findOne);

  // Update a Match with id
  router.put("/:id", matches.update);

  // Delete a Match with id
  router.delete("/:id", matches.delete);

  // Create a new Match
  router.delete("/", matches.deleteAll);

  app.use("/api/matches", router);
};
