module.exports = (app) => {
  const nssi = require("../controllers/nssi.controller.js");

  var router = require("express").Router();

  router.post("/submit", nssi.submit);
  router.get("/token", nssi.token);

  router.get("/status/:id", nssi.status);
  router.get("/results/:id", nssi.results);

  // router.delete("/delete", nssi.delete);

  app.use("/api/nssi", router);
};
