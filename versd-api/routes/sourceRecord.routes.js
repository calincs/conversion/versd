module.exports = app => {
    const sourceRecords = require("../controllers/sourceRecord.controller.js");

    var router = require("express").Router();

    // Create a new Job
    router.post("/", sourceRecords.create);

    // Retrieve all Jobs
    router.get("/", sourceRecords.findAll);

    // Retrieve a single Job with id
    router.get("/:id", sourceRecords.findOne);

    // Update a Job with id
    router.put("/:id", sourceRecords.update);

    // Delete a Job with id
    router.delete("/:id", sourceRecords.delete);

    // Create a new Job
    router.delete("/", sourceRecords.deleteAll);

    app.use('/api/sourceRecords', router);
};