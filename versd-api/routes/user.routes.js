/** @format */

module.exports = (app) => {
  const users = require("../controllers/user.controller.js");

  var router = require("express").Router();

  // Create a new Job
  router.post("/", users.create);

  // Retrieve all Jobs
  router.get("/", users.findAll);

  // Retrieve a single Job with id
  router.get("/:id", users.findOne);

  // Update a Job with id
  router.put("/:id", users.update);

  // Delete a Job with id
  router.delete("/:id", users.delete);

  // Create a new Job
  router.delete("/", users.deleteAll);

  app.use("/api/users", router);
};
