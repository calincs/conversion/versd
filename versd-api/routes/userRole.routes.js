/** @format */

module.exports = (app) => {
  const userRoles = require("../controllers/userRole.controller.js");

  var router = require("express").Router();

  // Create a new User Role
  router.post("/roles", userRoles.create);

  // Retrieve all User Roles
  router.get("/roles", userRoles.findAll);

  // Retrieve a single user role with id
  router.get("/:userId/roles/:jobId", userRoles.findOne);

  // Retrieve a user with any roles
  router.get("/:userId/roles", userRoles.findAllByUser);

  // Retrieve a user with any roles
  router.get("/roles/:role", userRoles.findAllByRole);

  // Update a Job with id
  router.put("/:userId/roles/:jobId", userRoles.update);

  // Delete a Job with id
  router.delete("/:userId/roles/:jobId", userRoles.delete);

  // Create a new Job
  router.delete("/roles", userRoles.deleteAll);

  app.use("/api/users", router);
};
