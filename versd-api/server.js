/** @format */

const express = require("express");
const cors = require("cors");

const app = express();
const db = require("./models");
// const seeders = require("./models/seed");

// console.log("Connecting to database " + process.env.DATABASE_URL);
// db.sequelize
//   .sync({ force: true })
//   .catch((err) => console.log(err));
//   .then((result) => {
//     seeders.seedUsers();
//     seeders.seedJobs();
//     console.log("Database synchronized");
//   })

var corsOptions = {
  origin: "*",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};
app.use(cors(corsOptions));

// parse requests of content-type - application/json + application/x-www-form-urlencoded
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

require("./routes/userRole.routes")(app);
require("./routes/user.routes")(app);
require("./routes/job.routes")(app);
require("./routes/match.routes")(app);
require("./routes/sourceRecord.routes")(app);
require("./routes/candidateMatch.routes")(app);
require("./routes/authorityMapping.routes")(app);
require("./routes/nssi.routes")(app);
require("./routes/counts.routes")(app);

app.get("*", (req, res) =>
  res.status(200).send({
    message: "VERSD API",
  }),
);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
