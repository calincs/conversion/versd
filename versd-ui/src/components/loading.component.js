/** @format */

// Loading.js
import React from "react";

const Loading = () => {
  return (
    <div className="loading">
      <i className="pi pi-spin pi-spinner" style={{ fontSize: "2rem" }}></i>
    </div>
  );
};

export default Loading;
