/** @format */
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { Divider } from "primereact/divider";

const relatedWorksTable = (name) => {
  <div className="related-works-table">
    <Divider type="solid" align="center" />

    <h2> Related Works by {name}</h2>
    <DataTable className="related-works-table">
      <Column selectionMode="multiple" headerStyle={{ width: "3em" }}></Column>

      <Column field="Title" header="Title"></Column>
      <Column field="Author Name" header="Author Name"></Column>
    </DataTable>
  </div>;
};
