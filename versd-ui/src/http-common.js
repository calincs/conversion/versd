import axios from "axios";
// initializes axios with HTTP base Url and headers

export default axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    "Content-type": "application/json",
  },
});
