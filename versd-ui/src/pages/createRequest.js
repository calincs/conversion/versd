/** @format */

import JobSubmissionForm from "../components/jobForm/jobSubmissionForm.component";

const NewRequest = () => {
  return <JobSubmissionForm />;
};

export default NewRequest;
