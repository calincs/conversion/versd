/** @format */

import http from "../http-common";

class MatchDataService {
  create(data) {
    return http.post("/matches", data);
  }

  getAll() {
    return http.get("/matches");
  }

  get(id) {
    return http.get(`/matches/${id}`);
  }

  update(id, data) {
    return http.put(`/matches/${id}`, data);
  }

  delete(id) {
    return http.delete(`/matches/${id}`);
  }

  deleteAll() {
    return http.delete(`/matches`);
  }

  findByStatus(status) {
    return http.get(`/matches?status=${status}`);
  }
  findBySourceRecord(sourceRecordId) {
    return http.get(`/matches?sourceRecordId=${sourceRecordId}`);
  }
}

const MatchService = new MatchDataService();
export default MatchService;
